# Crawly Challenge

O desafio: encontrar “a resposta”, utilizando um crawler.

“A resposta” se encontra atrás do clique de um botão, aqui: http://applicant-test.us-east-1.elasticbeanstalk.com/

Você deverá fazer um crawler que emita requisições e leia as respostas, em baixo nível (sem emuladores de browser, como puppeteer, selenium, phantomjs, etc). Esse crawler deve ser capaz de ler “a resposta" produzida e escrevê-la na tela.

Você tem total liberdade para usar a linguagem que se sentir mais confortável. Pode utilizar quaisquer frameworks/bibliotecas que quiser.

Também tem total liberdade para definir como disponibilizar o código da resposta desse desafio (git, zip, etc).


## Build Environment

```console
user@console:~$ git clone https://bitbucket.org/csazeredodev/crawly-challenge.git /to/some/path
user@console:~$ cd /to/some/path
user@console:~$ docker run -it --rm -v $PWD:/app --name applicant-test-container -p 8080:80 php:7.2 php -S 0.0.0.0:80 -t /app/public
```

## Access the Crawler

Acesse o browser no endereço http://localhost:8080, a resposta deverá ser um json contendo o token recebido e a resposta gerada

```javascript
{
  "token": "w614w15wy332z463uu18x4uw3521w04z",
  "answer": "47"
}
```


