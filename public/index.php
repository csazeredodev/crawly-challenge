<?php

require_once '../vendor/autoload.php';

use Challenge\Service\ApplicantTestCrawler;

$crawler = new ApplicantTestCrawler();
$token = $crawler->getTokenAndSaveCookieFile();
$answer = $crawler->sendReplacedTokenAndGetAnswer(replace_token($token));

json_response(compact('token', 'answer'));