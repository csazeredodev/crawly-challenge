<?php

if (! function_exists('cookie_path')) {
    function cookie_path($filename) : string
    {
        return realpath(__DIR__. '/../../cookies') . "/$filename";
    }
}

if (! function_exists('replace_token')) {
    function replace_token(string $token) : string
    {
        $letters = [
            'a' => 'z',
            'b' => 'y',
            'c' => 'x',
            'd' => 'w',
            'e' => 'v',
            'f' => 'u',
            'g' => 't',
            'h' => 's',
            'i' => 'r',
            'j' => 'q',
            'k' => 'p',
            'l' => 'o',
            'm' => 'n',
            'n' => 'm',
            'o' => 'l',
            'p' => 'k',
            'q' => 'j',
            'r' => 'i',
            's' => 'h',
            't' => 'g',
            'u' => 'f',
            'v' => 'e',
            'w' => 'd',
            'x' => 'c',
            'y' => 'b',
            'z' => 'a'
        ];

        $replacedToken = [];

        foreach (str_split($token) as $letter) {
            array_key_exists($letter, $letters) ? $replacedToken[] = $letters[$letter] : $replacedToken[] = $letter;
        };

        return implode("", $replacedToken);
    }
}

if (! function_exists('extract_token')) {
    function extract_token(string $html) : string
    {
        $dom = new \DOMDocument();
        $dom->loadHTML($html);

        return $dom->getElementsByTagName('input')
            ->item(0)
            ->attributes
            ->item(3)
            ->nodeValue;
    }
}

if (! function_exists('extract_answer')) {
    function extract_answer(string $html) : string
    {
        $dom = new \DOMDocument();
        $dom->loadHTML($html);

        return $dom->getElementById('answer')
            ->nodeValue;
    }
}

if (! function_exists('json_response')) {
    function json_response($data = []) : void
    {
        header("Content-type: application/json; charset=utf-8");
        echo \json_encode($data);
    }
}
