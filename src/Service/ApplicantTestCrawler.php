<?php

namespace Challenge\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\FileCookieJar;

/**
 * Class ApplicantTestCrawler
 * @package Challenge\Service
 */
class ApplicantTestCrawler
{
    CONST COOKIE_FILE_NAME = 'cookies.txt';
    CONST APPLICANT_TEST_URL = 'applicant-test.us-east-1.elasticbeanstalk.com';

    /**
     * @return string
     */
    public function getTokenAndSaveCookieFile() : string
    {
        if ($this->isThereAhCookieAlready()) {
            $this->removeCookie();
        }

        $cookieFile = $this->getCookieFile();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::APPLICANT_TEST_URL);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $output = curl_exec($ch);

        return extract_token($output);
    }

    /**
     * @param string $token
     * @return string
     */
    public function sendReplacedTokenAndGetAnswer(string $token) : string
    {
        $cookieFile = $this->getCookieFile();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
        curl_setopt($ch, CURLOPT_URL, self::APPLICANT_TEST_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "token=$token");
        curl_setopt($ch, CURLOPT_HTTPHEADER, [

                'Content-Type: application/x-www-form-urlencoded',
                "Referer: http://applicant-test.us-east-1.elasticbeanstalk.com/",
            ]
        );
        $answer = curl_exec($ch);

        return extract_answer($answer);
    }

    /**
     * @return bool
     */
    private function isThereAhCookieAlready() : bool
    {
        return file_exists(cookie_path(self::COOKIE_FILE_NAME));
    }

    /**
     * @return FileCookieJar
     */
    private function getCookieFile() : string
    {
        return cookie_path(self::COOKIE_FILE_NAME);
    }

    private function removeCookie() : void
    {
        unlink(cookie_path(self::COOKIE_FILE_NAME));
    }
}
